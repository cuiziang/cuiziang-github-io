---
layout: page
title: Certificate
permalink: /certificate/
---

#### General Tools
* [Version Control with Git](https://www.coursera.org/account/accomplishments/verify/7MKGPME4FBRY)

#### Java 
* [Java Programming: Solving Problems with Software](https://www.coursera.org/account/accomplishments/verify/5Y85NVNXLQFN)
* [Java Programming: Arrays, Lists, and Structured Data](https://www.coursera.org/account/accomplishments/verify/FTWWQ36DFHC6)

#### Python 
* [Programming for Everybody (Getting Started with Python)](https://www.coursera.org/account/accomplishments/verify/CA42CCFYFBCD)
* [Python Data Structures](https://www.coursera.org/account/accomplishments/verify/JVRBVW2PNK7Z)
* [Using Python to Access Web Data](https://www.coursera.org/account/accomplishments/verify/ZY3CCVM5VRP3)
* [Using Databases with Python](https://www.coursera.org/account/accomplishments/verify/7JMWM8NZQ8TZ)


#### PHP
* [Building Web Applications in PHP](https://www.coursera.org/account/accomplishments/verify/RFW6Y4ULCFM9)
* [PIntroduction to Structured Query Language (SQL))](https://www.coursera.org/account/accomplishments/verify/6SXRUP7XACAQ)
* [Building Database Applications in PHP](https://www.coursera.org/account/accomplishments/verify/P9EZ5RR8XD4U)

#### Front-end language and Front-end Framework 
* [Introduction to HTML5](https://www.coursera.org/account/accomplishments/verify/Q7LLPMNJ2TXJ)
* [Introduction to CSS3](https://www.coursera.org/account/accomplishments/verify/MM7LN6XKK7U4)
* [Interactivity with JavaScript](https://www.coursera.org/account/accomplishments/verify/MXK6WAUTUQ4D)
* [Front-End Web UI Frameworks and Tools: Bootstrap 4](https://www.coursera.org/account/accomplishments/verify/QDMB67KQMRGS)

#### Cisco Networking 
* [Internet Connection: How to Get Online?](https://www.coursera.org/account/accomplishments/certificate/2G76XLKF25AX)
* [Network Protocols and Architecture](https://www.coursera.org/account/accomplishments/certificate/CLDE9TD6WEWM)
* [Data Communications and Network Services](https://www.coursera.org/account/accomplishments/certificate/9LKPSAM6DMXW)

---------------

<sup>Specification: [PHP Specialization](https://www.coursera.org/account/accomplishments/specialization/8XHAC4GFUH2H), [Python Specialization](https://www.coursera.org/account/accomplishments/specialization/certificate/ZHZVWWZECFSZ)</sup>










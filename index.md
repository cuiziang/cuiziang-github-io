---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: home
---

Résumé
==================

> Also refer to the my [Certificates](/certificate) and [Github page](https://github.com/cuiziang).

### <span style="color: red">EDUCATION

#### **Algonquin College, Ottawa, Canada**  — Ontario College Diploma Program

<sup>_[www.algonquincollege.com](http://www.algonquincollege.com)_</sup>   <sup>_Sep 2017 - Present, Ottawa, Canada_</sup>

- Introduction: acquired workplace-ready programming languages and practical applications, and gain valuable real-world experience through a group software-development project.

#### **Ningbo University, Ningbo, China** — Bachelor of Business Laws 

<sup>_[iso.nbu.edu.cn](http://iso.nbu.edu.cn)_</sup>   <sup>_SEP 2006 - Jun 2010, Ningbo, China_</sup>

- Introduction: acquired a strong understanding of the Chinese business law, including its rules, agents, institutions and power structures. 

### <span style="color: red">PROGRAM-RELATED SKILLS</span>
- Front-end Language and Front-end Framework of Web Programming
  - HTML, CSS, Javascript, jQuery
  - Bootstrap
  - Angular
- Back-end language
  - Java (JavaFX, Processing)
  - Python (Flask, NumPy, Pandas, Matplotlib)
  - PHP
  - Node.js (Express, Mongoose)
- Data and Database
  - SQL (Postgres, MySQL, MongoDB)
  - GraphQL

### <span style="color: red">EXPERIENCE</span>

#### **Guotai Junan Securities Co.Ltd., Shanghai** — Business Data Analyst 

<sup>_[www.gtja.com](https://www.gtja.com/portal/channel/indexen.jhtml)_</sup>  <sup>_Jan 2014 - Oct 2017_</sup>

- Visualize financial data through graphical tools such as Matplotlib and PyGal.
Draft Business analysis report.
Make sure there was no financial and law risk to get listed on stock market based on .

### <span style="color: red">PERSONAL ATTRIBUTES</span>
* Fluent in English and Chinese
* Drivers License of Ontario
* Strong passion in learning new technologies, heavy user in MOOC (Coursera, Udacity, Datacamp).[^a]
* Not only programming knowledge but also business awareness.

---------------
[^a]: Reference on [certificare page](certificate).


